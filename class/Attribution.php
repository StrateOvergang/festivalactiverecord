<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Attribution
 *
 * @author $oDy
 */
class Attribution {

    public function setNombreChambres($nombreChambres) {
        $this->nombreChambres = $nombreChambres;
    }

    public function getEtablissement() {
        return $this->etablissement;
    }

    public function getGroupe() {
        return $this->groupe;
    }

    public function getNombreChambres() {
        return $this->nombreChambres;
    }

    public function save() {
        if ($this->newItem) {
            $query = Attribution::$insert;
        } else {
            $query = Attribution::$update;
        }
        $pdo = DataBaseAccess::open();
        $pdoStatement = $pdo->prepare($query);
        $idGroupe = $this->groupe->getId();
        $idEtab = $this->etablissement->getId();
        $nombreChambres = $this->nombreChambres;
        $pdoStatement->bindParam(":idGroupe", $idGroupe);
        $pdoStatement->bindParam(":idEtab", $idEtab);
        $pdoStatement->bindParam(":nombreChambres", $nombreChambres);
        $pdoStatement->execute();
        DataBaseAccess::close();
    }

    /*
    public static function fetchAllGroupeByEtablissement() {
        $collection = null;
        $pdo = DataBaseAccess::open();
        $pdoStatement = $pdo->query(Attribution::$select);
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        if ($record != false) {
            $idEtab = $record["idEtab"];
            $etab = Etablissement::fetch($record["idEtab"]);
            $collection[$etab->getId()]['etablissement'] = $etab;
            $collection[$etab->getId()]['groupes'] = array();
            while ($record != false) {
                if ($idEtab != $record["idEtab"]) {
                    $etab = Etablissement::fetch($record["idEtab"]);
                    $collection[$etab->getId()]['etablissement'] = $etab;
                    $collection[$etab->getId()]['groupes'] = array();
                    $collection[$etab->getId()]['groupes'][$record["nombreChambres"]] = Groupe::fetch($record["idGroupe"]);
                    $idEtab = $etab->getId();
                } else {
                    $collection[$etab->getId()]['groupes'][$record["nombreChambres"]] = Groupe::fetch($record["idGroupe"]);
                }
                $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
            }
        }
        DataBaseAccess::close();
        return $collection;
    }
     
     */

    public static function fetch($idEtab, $idGroupe) {
        $pdo = DataBaseAccess::open();
        $pdoStatement = $pdo->prepare(Attribution::$selectById);
        $pdoStatement->bindParam(":idGroupe", $idGroupe);
        $pdoStatement->bindParam(":idEtab", $idEtab);
        $pdoStatement->execute();
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        var_dump($pdoStatement->errorInfo());
        $attribution = null;
        if ($record != false) {
            $attribution = Attribution::arrayToAttribution($record);
        }
        return $attribution;
    }

    public static function fetchAll() {
        $collectionAttribution = null;
        $pdo = DataBaseAccess::open();
        $pdoStatement = $pdo->query(Attribution::$select);
        $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($recordSet as $record) {
            $collectionAttribution[] = Attribution::arrayToAttribution($record);
        }
        DataBaseAccess::close();
        return $collectionAttribution;
    }

    private static function arrayToAttribution(array $record) {
        $a = new Attribution();
        $a->newItem = false;
        $a->groupe = Groupe::fetch($record["idGroupe"]);
        $a->etablissement = Etablissement::fetch($record["idEtab"]);
        $a->nombreChambres = $record["nombreChambres"];
        return $a;
    }

    private static $select = "select * from attribution order by  idEtab";
    private static $selectById = "select * from attribution where idGroupe=:idGroupe and idEtab=:idEtab order by IdEtab";
    private static $update = "update attribution set idGroupe=:idGroupe, idEtab=:idEtab,nombreChambres=:nombreChambres where idGroupe=:idGroupe and idEtab=:idEtab";
    private static $insert = "insert into attribution (idGroupe,idEtab,nombreChambres) values (:idGroupe,:idEtab,:nombreChambres)";
    private $etablissement;
    private $groupe;
    private $nombreChambres;
    private $newItem = true;

}
