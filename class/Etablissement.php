<?php

/**
 * Représente un établissement
 *
 * @author $oDy
 */
class Etablissement {

    public function getNomResponsable() {
        return $this->nomResponsable;
    }

    public function getPrenomResponsable() {
        return $this->prenomResponsable;
    }

    public function getId() {
        return $this->id;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getAdresseRue() {
        return $this->adresseRue;
    }

    public function getCodePostal() {
        return $this->codePostal;
    }

    public function getVille() {
        return $this->ville;
    }

    public function getTel() {
        return $this->tel;
    }

    public function getAdresseElectronique() {
        return $this->adresseElectronique;
    }

    public function getType() {
        return $this->type;
    }

    public function getCiviliteResponsable() {
        return $this->civiliteResponsable;
    }

    public function getNombreChambresOffertes() {
        return $this->nombreChambresOffertes;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function setAdresseRue($adresseRue) {
        $this->adresseRue = $adresseRue;
    }

    public function setCodePostal($codePostal) {
        $this->codePostal = $codePostal;
    }

    public function setVille($ville) {
        $this->ville = $ville;
    }

    public function setTel($tel) {
        $this->tel = $tel;
    }

    public function setAdresseElectronique($adresseElectronique) {
        $this->adresseElectronique = $adresseElectronique;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function setCiviliteResponsable($civiliteResponsable) {
        $this->civiliteResponsable = $civiliteResponsable;
    }

    public function setNomResponsable($nomResponsable) {
        $this->nomResponsable = $nomResponsable;
    }

    public function setPrenomResponsable($prenomResponsable) {
        $this->prenomResponsable = $prenomResponsable;
    }

    public function setNombreChambresOffertes($nombreChambresOffertes) {
        $this->nombreChambresOffertes = $nombreChambresOffertes;
    }

    public function setId($id) {
        if ($this->getId() == null) {
            $this->tempId = $id;
        } else {
            $this->id = $id;
        }
    }

    public static function fetchAll() {
        $collectionEtablissement = null;
        $pdo = DataBaseAccess::open();
        $pdoStatement = $pdo->query(Etablissement::$select);
        $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($recordSet as $record) {
            $collectionEtablissement[] = Etablissement::arrayToEtablissement($record);
        }
        DataBaseAccess::close();
        return $collectionEtablissement;
    }

    public static function fetch($id) {
        $pdo = DataBaseAccess::open();
        $pdoStatement = $pdo->prepare(Etablissement::$selectById);
        $pdoStatement->bindParam(":id", $id);
        $pdoStatement->execute();
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        $etablissement = null;
        if ($record != false) {
            $etablissement = Etablissement::arrayToEtablissement($record);
        }
        return $etablissement;
    }

    public static function arrayToEtablissement(array $record) {
        $e = new Etablissement();
        $e->id = $record["id"];
        $e->nom = $record["nom"];
        $e->adresseElectronique = $record["adresseElectronique"];
        $e->adresseRue = $record["adresseRue"];
        $e->codePostal = $record["codePostal"];
        $e->ville = $record["ville"];
        $e->tel = $record["tel"];
        $e->type = $record["type"];
        $e->civiliteResponsable = $record["civiliteResponsable"];
        $e->nombreChambresOffertes = $record["nombreChambresOffertes"];
        $e->prenomResponsable = $record["prenomResponsable"];
        $e->nomResponsable = $record["nomResponsable"];
        return $e;
    }

    public static function delete($id) {
        $pdo = DataBaseAccess::open();
        $pdoStatement = $pdo->prepare(Etablissement::$delete);
        $pdoStatement->bindParam(":id", $id);
        $pdoStatement->execute();
        DataBaseAccess::close();
    }

    public function save() {

        if ($this->id == null) {
            $this->id = $this->tempId;
            $query = Etablissement::$insert;
        } else {
            $query = Etablissement::$update;
        }
        $pdo = DataBaseAccess::open();
        $pdoStatement = $pdo->prepare($query);
        $pdoStatement->bindParam(":id", $this->id);
        $pdoStatement->bindParam(":nom", $this->nom);
        $pdoStatement->bindParam(":adresseRue", $this->adresseRue);
        $pdoStatement->bindParam(":codePostal", $this->codePostal);
        $pdoStatement->bindParam(":ville", $this->ville);
        $pdoStatement->bindParam(":tel", $this->tel);
        $pdoStatement->bindParam(":adresseElectronique", $this->adresseElectronique);
        $pdoStatement->bindParam(":type", $this->type);
        $pdoStatement->bindParam(":civiliteResponsable", $this->civiliteResponsable);
        $pdoStatement->bindParam(":nomResponsable", $this->nomResponsable);
        $pdoStatement->bindParam(":prenomResponsable", $this->prenomResponsable);
        $pdoStatement->bindParam(":nombreChambresOffertes", $this->nombreChambresOffertes);
        $pdoStatement->execute();
        DataBaseAccess::close();
    }

    private static $selectById = "select * from etablissement where id = :id";
    private static $select = "select * from etablissement";
    private static $insert = "insert into etablissement (id,nom,adresseRue,codePostal,ville,tel,adresseElectronique,type,civiliteResponsable,nomResponsable,prenomResponsable,nombreChambresOffertes) values (:id,:nom,:adresseRue,:codePostal,:ville,:tel,:adresseElectronique,:type,:civiliteResponsable,:nomResponsable,:prenomResponsable,:nombreChambresOffertes)";
    private static $update = "update Etablissement set nom=:nom,adresseRue=:adresseRue,codePostal=:codePostal,ville=:ville,tel=:tel,adresseElectronique=:adresseElectronique,type=:type,civiliteResponsable=:civiliteResponsable,nomResponsable=:nomResponsable,prenomResponsable=:prenomResponsable,nombreChambresOffertes=:nombreChambresOffertes where id=:id";
    private static $delete = "delete from etablissement where id=:id";
    private $id;
    private $tempId;
    private $nom;
    private $adresseRue;
    private $codePostal;
    private $ville;
    private $tel;
    private $adresseElectronique;
    private $type;
    private $civiliteResponsable;
    private $nomResponsable;
    private $prenomResponsable;
    private $nombreChambresOffertes;

}
