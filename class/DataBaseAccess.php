<?php

class DataBaseAccess {

    private static $pdo;

    private static function initPdo() {
        try {
            DataBaseAccess::$pdo = new PDO('mysql:host=localhost;dbname=festival', 'root', 'root', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
        } catch (PDOException $err) {
            $messageErreur = $err->getMessage();
            error_log($messageErreur);
        }
    }
    
    public static function open() {
        if (DataBaseAccess::$pdo == null) {
            DataBaseAccess::initPdo();
            if (DataBaseAccess::$pdo == null) {
                throw new Exception("NoDataBaseConnection");
            }
        }
        return DataBaseAccess::$pdo;
    }

    public static function close() {
        DataBaseAccess::$pdo = null;
    }

}

?>