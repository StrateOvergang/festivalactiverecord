<?php

/**
 * Description of Groupe
 *
 * @author $oDy
 */
class Groupe {

    public function getId() {
        return $this->id;
    }

    public function getNom() {
        return $this->nom;
    }

    public function getIdentiteResponsable() {
        return $this->identiteResponsable;
    }

    public function getAdressePostale() {
        return $this->adressePostale;
    }

    public function getNombrePersonnes() {
        return $this->nombrePersonnes;
    }

    public function getNomPays() {
        return $this->nomPays;
    }

    public function getHebergement() {
        return $this->hebergement;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setNom($nom) {
        $this->nom = $nom;
    }

    public function setIdentiteResponsable($identiteResponsable) {
        $this->identiteResponsable = $identiteResponsable;
    }

    public function setAdressePostale($adressePostale) {
        $this->adressePostale = $adressePostale;
    }

    public function setNombrePersonnes($nombrePersonnes) {
        $this->nombrePersonnes = $nombrePersonnes;
    }

    public function setNomPays($nomPays) {
        $this->nomPays = $nomPays;
    }

    public function setHebergement($hebergement) {
        $this->hebergement = $hebergement;
    }

    public static function fetchAll() {
        $collectionGroupe = null;
        $pdo = DataBaseAccess::open();//récupération d'un objet PDO
        $req = Groupe::$select;//affecter le contenu de la variable $select de la classe
        $pdoStatement = $pdo->query($req);//exécution de la requete par l'objet PDO
        $recordSet = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);//récupération d'un tableau de tableau
        //$recordSet ==> tableau (tableau de tableau) d'enregistrements
        //$record ==> un enregistrement (type array)
        foreach ($recordSet as $record) {
            $collectionGroupe[] = Groupe::arrayToGroupe($record);
        }
        return $collectionGroupe;
    }

    public static function fetch($id){
        $groupe = null;
        $pdo = DataBaseAccess::open();
        $pdoStatement = $pdo->prepare(Groupe::$selectById);
        $pdoStatement->bindParam(":id", $id);
        $pdoStatement->execute();
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        if ($record != false) {
            $groupe = Groupe::arrayToGroupe($record);
        }
        return $groupe;
    }
    
    private static function arrayToGroupe(array $record) {
        $g = new Groupe();
        $g->id = $record["id"];
        $g->nombrePersonnes = $record["nombrePersonnes"];
        $g->nom = $record["nom"];
        $g->identiteResponsable = $record["identiteResponsable"];
        $g->nomPays = $record["nomPays"];
        $g->adressePostale = $record["adressePostale"];
        $g->hebergement = $record["hebergement"];
        return $g ;
    }

    private static $select = "select * from groupe";
    private static $selectById = "select * from groupe where id = :id";
    
    private $id;
    private $nom;
    private $identiteResponsable;
    private $adressePostale;
    private $nombrePersonnes;
    private $nomPays;
    private $hebergement;

}
