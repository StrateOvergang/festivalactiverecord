<?php
include_once '_debut.inc.php';

if (!isset($_REQUEST["numEtablissement"])) {
    header("location: consultationEtablissements.php");
}

$id = $_REQUEST['numEtablissement'];
$etablissement = Etablissement::fetch($id);

?>

<div class="container">
    <div class="row ">
        <?php        include_once 'menuGauche.inc.php';?>

        <div class="col-md-7 ">
            <article>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo $etablissement->getNom(); ?></h3>
                    </div>
                    <div class="panel-body">
                        <p> <?php echo $etablissement->getAdresseRue(); ?> </p>
                        <p> <?php echo $etablissement->getCodePostal() ?> <?php echo $etablissement->getVille(); ?> </p>
                        <p>
                            <span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span>
                               <?php echo $etablissement->getTel()?>
                        </p>
                        <address><?php echo $etablissement->getAdresseElectronique() ?></address>
                        <p>
                            <?php if ($etablissement->getType() == 1): ?>
                                    Etablissement scolaire 
                                <?php else: ?>
                                    Autre établissement
                                <?php endif; ?>
                        </p>
                        <p>
                             <?php echo $etablissement->getCiviliteResponsable() . " " . $etablissement->getPrenomResponsable() . " " . $etablissement->getNomResponsable(); ?>
                        </p>
                        <p> <?php echo $etablissement->getNombreChambresOffertes(); ?> chambre(s)proposées </p>
                    </div>
                </div> 
            </article>
        </div>
    </div>



</div> <!-- /container -->







<?php include("_fin.inc.php"); ?>