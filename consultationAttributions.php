<?php
include_once '_debut.inc.php';
?>

<!-- Une div contenant la class "container" préfixe obligatoirement les lignes (div de class=row) -->
<div class="container">
    <!-- ligne principale -->
    <div class="row "> 

        <?php include_once 'menuGauche.inc.php'; ?>

        <!-- deuxième colonne (s'étend sur 7 colonnes sur 12 possibles à partir de la 3) -->
        <div class="col-md-9 ">
            <br />
            <!-- une ligne dans une colonne -->

            <?php
            $listeAttributions = Attribution::fetchAll();


            while (current($listeAttributions) != false) :
                $idEtab = current($listeAttributions)->getEtablissement()->getId();
                ?>
                <table class="table table-condensed">
                    <caption>
                        <?php echo current($listeAttributions)->getEtablissement()->getNom(); ?>
                    </caption> 
                    <th>Nom Groupe</th>
                    <th>Nombre de chambres</th>
                    <?php do { ?>      
                        <tr class="active">                           
                            <td >
                                <?php
                                echo current($listeAttributions)->getGroupe()->getNom();
                                ?> 
                            </td>
                            <td>
                                <form method="post" class="form-inline" action="consultationAttributions.traitement.php">
                                    <div class="form-group">
                                        <input type="hidden" name="idEtab" name="idEtab" value="<?php echo current($listeAttributions)->getEtablissement()->getId(); ?>"/>
                                        <input type="hidden" name="idGroupe" name="idGroupe" value="<?php echo current($listeAttributions)->getGroupe()->getId(); ?>"/>
                                        <input type="text" class="form-control" name="nombreChambres" id="nombreChambres" placeholder="Saisir ou modifier le nombre de chambres affectés" value="<?php echo current($listeAttributions)->getNombreChambres();
                                ?>">
                                    </div>
                                    <button type="submit" class="btn btn-default">Valider</button>
                                </form>
                            </td>                            
                        </tr>
                        <?php
                    } while (next($listeAttributions) != false && $idEtab == current($listeAttributions)->getEtablissement()->getId());
                endwhile;
                ?>
        </div>
    </div>



</div> <!-- /container -->




<?php include("_fin.inc.php"); ?>
