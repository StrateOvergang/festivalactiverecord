<?php

include_once '_debut.inc.php';

if (isset($_REQUEST)) {
    $etab = Etablissement::fetch($_REQUEST['id']);
    $etab->setId($_REQUEST['id']);
    $etab->setNom($_REQUEST['nom']);
    $etab->setAdresseRue($_REQUEST['adresseRue']);
    $etab->setCodePostal($_REQUEST['codePostal']);
    $etab->setVille($_REQUEST['ville']);
    $etab->setTel($_REQUEST['tel']);
    $etab->setAdresseElectronique($_REQUEST['adresseElectronique']);
    $etab->setType($_REQUEST['type']);
    $etab->setCiviliteResponsable($_REQUEST['civiliteResponsable']);
    $etab->setNomResponsable($_REQUEST['nomResponsable']);
    $etab->setPrenomResponsable($_REQUEST['prenomResponsable']);
    $etab->setNombreChambresOffertes($_REQUEST['nombreChambresOffertes']);
    $etab->save();
    $id = $etab->getId();
    header("location: modificationEtablissement.php?numEtablissement=$id");
} else {
    header("location: consultationEtablissement.php");
}
?>