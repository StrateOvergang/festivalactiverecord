<?php
include_once '_debut.inc.php';
?>

<!-- Une div contenant la class "container" préfixe obligatoirement les lignes (div de class=row) -->
<div class="container">
    <!-- ligne principale -->
    <div class="row "> 
       
      <?php        include_once 'menuGauche.inc.php';?>

        <!-- deuxième colonne (s'étend sur 7 colonnes sur 12 possibles à partir de la 3) -->
        <div class="col-md-7 ">
            <br />
            <!-- une ligne dans une colonne -->
            <div class="row">
                <?php
                $listeEtablissements = Etablissement::fetchAll();
                
                if ($listeEtablissements != false):
                    foreach ($listeEtablissements as $etablissement):
                        ?> 
                
                        <div class="col-md-6">
                            <article class="panel panel-default articleEtablissement bgColorTheme">
                                <p> Nom : <?php echo $etablissement->getNom() ?>  </p>
                                <p> Adresse : <?php echo $etablissement->getAdresseRue() ?> </p>
                                <p> Code Postal : <?php echo $etablissement->getCodePostal() ?> </p>
                                <p> Ville : <?php echo $etablissement->getVille() ?> </p>
                                <ol class="breadcrumb">
                                    <li> 
                                        <a href="modificationEtablissement.php?numEtablissement=<?php echo $etablissement->getId() ?>">modifier
                                        </a>
                                    </li>
                                    <li> 
                                        <a href="detailEtablissement.php?numEtablissement=<?php echo $etablissement->getId() ?>">Détail
                                        </a>
                                    </li>
                                    <li class="active">
                                        <a href="suppressionEtablissement.php?numEtablissement=<?php echo $etablissement->getId() ?>">Suppression
                                        </a>
                                    </li>
                                </ol>
                            </article>
                        </div>

                    <?php endforeach; ?>                
                <?php endif; ?> 



            </div>
        </div>
    </div>
    


</div> <!-- /container -->




<?php include("_fin.inc.php"); ?>
