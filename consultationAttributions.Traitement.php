12<?php

include_once '_debut.inc.php';

if (isset($_POST)) {
    $idGroupe = $_POST["idGroupe"];
    $idEtab = $_POST["idEtab"];
    $nombreChambres = $_POST["nombreChambres"];
    $attribution = Attribution::fetch($idEtab,$idGroupe);
    $attribution->setNombreChambres($nombreChambres);
    $attribution->save();
}

header("location:consultationAttributions.php");

